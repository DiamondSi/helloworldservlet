package ru.academits.dubchak.helloworldservlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloWorldServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String textToSend = "Hello World / GET!";
        resp.getWriter().write(textToSend);
        resp.getWriter().flush();
        resp.getWriter().close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String textToSend = "Hello World / POST!";
        resp.getWriter().write(textToSend);
        resp.getWriter().flush();
        resp.getWriter().close();
    }
}
